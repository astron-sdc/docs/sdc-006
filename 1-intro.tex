\section{Introduction}
\label{sec:intro}

This document lays out the vision for the ASTRON \gls{SDC}. Its intended audiences are:

\begin{enumerate}

  \item The teams building and operating the \gls{SDC}, which will use this vision as a frame of reference for prioritising and communicating about their work;

  \item ASTRON leadership and other stakeholders, to provide them with a high-level overview of the goals of the \gls{SDC} and how they are being addressed.

\end{enumerate}

The vision establishes the general terms of reference for the \gls{SDC} effort.
It focuses upon the \emph{what} of the \gls{SDC}: what is that we are building, and how do we know that it will enable us to fulfil the mission that we have been given?
It then goes on to complete the picture by describing the essential background:
who is responsible for building it?
How will it be operated?
How will it be resourced?


It follows that this vision is distinct from a timeline or roadmap for the availability of concrete features within the \gls{SDC}: roadmaps, based an ongoing assessment of technical need and community engagement --- following the model described in this document --- and the available developer effort, will follow later.
Similarly, it is not practical for this document to provide detailed information about all technical, logistical, managerial, and operational aspects of the \gls{SDC}.
It will therefore act as the root of a document tree, with other documents --- including management plans, requirements documentation, and operational concepts --- ultimately deriving from this one.

The material in this document derives from the guidance issued by ASTRON leadership to the authors about the aims of the \gls{SDC}, as summarized in \cref{sec:mission}.
We have used this guidance as the basis of an analysis of use cases and the capabilities that can best serve them.
This analysis is briefly summarized in \cref{sec:analysis}, with much more extensive documentation in \Cref{sec:users,sec:features}.
The capabilities thus identified are used to motivate the overall design of the \gls{SDC} facility, which is summarized in \cref{sec:product}.
In \cref{sec:sizing} we briefly consider whether it is plausible to develop a system at the scale --- in terms of compute and storage --- required to meaningfully address user needs.

Having thus established our goals, the remainder of the document then addresses the interactions between the facility and its environment.
Specifically, in \cref{sec:community} we describe how the \gls{SDC} will relate to the wider community: who will have access? what will their rights be? how will the facility incorporate feedback from its users and other stakeholders?
Finally, in \cref{sec:sustainability}, we describe how the project will be made sustainable, both in terms of its environmental impacts, and of its place within the wider landscape of astronomical software and data management systems.

This is a ``living'' document, which we expect to develop and refine as our plans for the \gls{SDC} mature, and as the technical and scientific landscape in which we are operating evolves.
However, it will be the subject of strict change control, to ensure that changes to the vision are coherent, communicated to all stakeholders, and appropriately flowed-down to lower-level documentation.
Feedback from the community and from all stakeholders will be sought during this process to make sure that the services offered by the \gls{SDC} meet their needs.
