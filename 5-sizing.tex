\section{Sizing and Resourcing}
\label{sec:sizing}

Detailed sizing of the ultimate \gls{SDC} system is not yet completed, and ultimately will be outside the scope of this document.
This section aims to present a high-level overview of our current understanding to serve as an input to the design of the \gls{SDC} system.

\subsection{Data Storage}

The \gls{SDC} will store and offer to the community multiple petabytes of data (as described in \Cref{sec:features:raw,sec:features:srdp,sec:features:simple}).
Current data holdings, and their predicted growth rates, are estimated as:

\begin{itemize}
\item 50\,PB of \gls{LOFAR} data distributed over three archive sites, growing at a rate of about 7\,PB a year;
\item 4\,PB of Apertif data, which will increase to 7.5\,PB by the end of the Apertif surveys.
\end{itemize}

In future, we expect the \gls{LOFAR}2.0 upgrade to substantially increase --- perhaps by a factor of two --- the current data rate.
It has not been established whether this will directly translate into a doubling of the request for storage at the archival sites --- both technical and scientific concerns will impact on this --- but a substantial increase in storage volume should be expected.
In total, though, the volume of data originating from ASTRON-managed instrumentation over the foreseeable future is unlikely to exceed several tens of PB; while this requires substantial investment, it is not technically challenging given the current capabilities of ASTRON systems.

However, in addition to this the Dutch \gls{SRC} will also host data from the \gls{SKA}, starting during the commissioning phase in 2022--2023 and continuing into production and beyond from 2027--2028.
Although the storage requirements for the \gls{SRC} network have not yet been established, it is expected that in aggregate the total capacity of the \glspl{SRC} will grow at an annual rate of around 1\,EB by the time that the \gls{SKA} is fully operational.
This represents a change on a scale of orders-of-magnitude relative to the current system.
We believe the system described in \cref{sec:product} is scalable to address these demands, but we note that \emph{substantial} investment in infrastructure will be required to host these data volumes.
Under the umbrella of the FuSE Project \autocite{2019-FuSE-Proposal}, the \gls{SDC} team will collaborate with our colleagues at SURF and Nikhef in infrastructure and technology development targeting the \gls{SKA}.

\subsection{Compute Capacity}

Per \Cref{sec:features:srdp}, compute capacity will be needed to generate a range of \gls{SRDP}.
It is expected that this work will be undertaken for the vast majority of raw and instrumental data received by the \gls{SDC}.
Estimates of the total capacity required are uncertain, as the pipelines are still under development.
Detailed sizing estimates will therefore continue to evolve with experience.
However, some preliminary estimates can be made based on existing \gls{LOFAR} data processing.
Specifically, The \gls{SDC} will be used to generate \gls{SRDP} for:

\begin{itemize}
\item All data that has been collected in the \gls{LOFAR} \gls{LTA} since the start of LOFAR operations;
\item Data generated from new observations.
\end{itemize}

Based on the estimates made for the initial phases of the ongoing \Acrshort{LDV} project, about 3--4 million core hours will be needed to process all LOFAR data through direction-independent calibration.
Direction-dependent calibration and imaging may be a few times more demanding, but we note that the final performance will also depend on code optimization that is currently underway.
Based on the type of observing requests currently received, we extrapolate future needs to amount to around 5 million core hours per observing cycle.

Eventually, science teams and individual users will also be using the \gls{SDC} services for processing and data analysis (\Cref{sec:features:standardpipes,sec:features:custompipes,sec:features:interactive}).
The size of the service that we intend to offer in this area is currently uncertain.
However, we note that for the initial operational period in the \Acrshort{EGI-ACE} Project a conservative estimate was made of supporting 10 science teams with 5 million core hours in total (500,000 core hours per team).
This model, which we expect to apply initially, will need to evolve and resources will need to be divided further to allow more individual users (up to 1,000 astronomers, based on our current estimates) to be able to use the SDC services.

\subsection{Resource Acquisition}

Resources to operate the \gls{SDC} will be sourced from facilities distributed over multiple physical locations.
In particular, from the outset the facility should look to support operations over the current \gls{LTA} sites at SURF (Amsterdam), Jülich, and Poznań.
We expect this model, in which storage and compute systems used by the \gls{SDC} are partially provided as in-kind contributions to the \gls{ILT} and partially directly funded by ASTRON, to continue for the foreseeable future.
We will also investigate cost-competitive alternatives where appropriate.

To date, computing resources at SURF have been secured for \gls{SDC} processing through project funding.
Although this has been successful to date, \emph{we anticipate a need to secure structural funding for \gls{SDC} operations in support of future \gls{LOFAR} observing cycles}.
The \gls{SDCO} team will engage with the \gls{ILT} Board and, where appropriate, other stakeholders to address these needs.

\subsection{Quotas and Resource Allocation}
\label{sec:sizing:quotas}

The \gls{SDC} will make both storage and computing resources available to end users (\Cref{sec:features:standardpipes,sec:features:custompipes,sec:features:interactive}).
In both cases, it is an operational and practical necessary that these resources be limited to avoid resource exhaustion which may interfere with other users' activities or even with the operation of the facility itself.

Resource allocation will be a matter of policy which is set by \gls{SDC} leadership in conjunction with, and under the guidance of, various stakeholders, as discussed in \cref{sec:community:engagement}.
It is outside the scope of this document to define that policy; we simply note here that mechanisms for enforcing policy must be provided.
