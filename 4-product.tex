\section{System Design}
\label{sec:product}

Providing the capabilities arrived at through the process described in \cref{sec:analysis} requires the establishment of a cutting-edge data archiving and processing facility: the \gls{SDC} itself.
This section provides a sketch of the service-driven architecture which we will use to drive initial \gls{SDC} development.
Note that the services described here are designed to provide the capabilities identified in \cref{sec:analysis} (which, in turn, derive from the more extensive analysis in the appendices).

Note that the architecture described here is expected to evolve with time.
As services are deployed; as new capabilities are developed, both inside and outside ASTRON; and as community expectations and needs change, the \gls{SDC} architecture will be updated to keep pace.
In particular, and as discussed in \cref{sec:mission}, a core part of the \gls{SDC}'s goal is to contribute to and learn from the \gls{SRC} effort and, ultimately, to operate an \gls{SRC}: it is therefore reasonable to expect convergent evolution of the \gls{SDC} and \gls{SRC} architectures over the coming years.
However, we believe the design described here provides a solid basis for future development.

The architecture described here is necessarily superficial: it provides a high-level overview to help with orientation, but it is expected that the reader will refer to lower-level design documentation (as it becomes available) for detailed design decisions.

\subsection{Architecture and Components}
\label{sec:product:arch}

The \gls{SDC} is conceived as a single point of contact for all of the functionality made available to end users.
It is structured as an interlocking system of services.
Each service will provide a set of capabilities to the \gls{SDC} system, while interoperating with the other services to form the complete \gls{SDC} service package.
There are three major services which form the key user-facing capabilities of the \gls{SDC}.
These services are shown schematically in \cref{fig:overview}, and described briefly below:
\begin{itemize}

\item The Data Product Repository, which provides a central location for storing data at all levels of processing.
\item The Data Processing Service, which fetches data from the Repository, executes one or more processing steps, and stores the result back to the Repository.
\item The Portal, which provides a web-based interface through which users will interact with all \gls{SDC} capabilities.

\end{itemize}

These major services rely upon a number of \emph{ancillary services} which provide data management and publication services, interfaces to external instrumentation, and user and account management.
A non-exhaustive list includes:

\begin{itemize}
\item The Application Repository, which will store and make available software components for use in Data Processing Service and Portal.
\item The Staging, Data Transfer, and Archiver services, which make data available to the Data Processing Service and ingest results back into the Data Product Repository.
\item The Science Data Repository and Virtual Observatory services, which provide metadata and interfaces that enable the Data Repository to be searched, indexed, and made accessible using standard tooling, while ensuring that appropriate data access policies are enforced.
\item The Federated Authentication and Authorization Infrastructure and the Community Management Service, which provide control over user access rights.
\end{itemize}

These ancillary services provide key capabilities within the SDC are not therefore discussed in detail in this document, but will be described further in the SDC Architectural Design \autocite{SDC-008}.

A further set of user-facing capabilities are provided through \emph{support services}.
These are mechanisms by which users can obtain support, either from \gls{SDC} staff directly, from their peers, or simply by accessing documentation and other material.
In general, we do not expect to develop new software to address these needs; rather, off-the-shelf commercial or open-source tools (e.g. Jira\footnote{\url{https://www.atlassian.com/software/jira}}, Discourse\footnote{\url{https://www.discourse.org}}) will be deployed, and integrated with the \gls{SDC} Portal where appropriate.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{figures/SDC/sdc-architecture}
\end{center}
\caption{%
An overview of the \gls{SDC} system.
This figure shows the three major services of the \gls{SDC} and the relationships between them.
In the interests of clarity, not all ancillary and support services are shown, and some data and control flows are omitted.
}
\label{fig:overview}
\end{figure}

\subsubsection{Data Product Repository}
\label{sec:product:services:repo}

The Data Product Repository and associated services will provide robust, policy-based archival storage of bulk data products.
This includes both low-level (raw, instrumental) and advanced, science-ready data products (\Cref{sec:features:data}).
In addition, the Repository will be provide for database-style storage of source catalogues and other tabular data (\Cref{sec:features:standardpipes,sec:features:custompipes}).

The Repository will be structured so that scientists can refer to and access data by logical identifier, rather than physical location.
For example, users will select data based on properties like an observation identifier, rather than by a filesystem path.
This is not only more convenient for users, but also means that the underlying physical infrastructure can be abstracted away.

Abstraction of physical infrastructure makes it possible for the Repository to be spread over multiple physical archive sites.
This represents the present reality\footnote{Data holdings in Amsterdam, Jülich, and Poznań.}, and provides flexibility for future infrastructure acquisition.
These multiple sites will likely all provide somewhat different physical infrastructure.
In general, however, the Repository is expected to take an hierarchical approach to data storage, with most data products being archived on ``nearline'' systems, such as tape libraries, and staged to online services prior to processing.

The distributed nature of the Repository represents a challenge in terms of data locality: in general, it is cheaper and more efficient to process data close to where it is stored, rather than transmitting it over a long-haul network for analysis.
The Repository will therefore cooperate with the Data Processing Service and related ancillary services to route processing and analytics jobs to compute systems which are as close to the data as possible.

To facilitate data sharing (\Cref{sec:features:sharing}), the Repository will provide support for automatically associating \glspl{PID} with published data products.
Ancillary services will ensure that appropriate data published to the Repository is made available to the \gls{VO} (\Cref{sec:features:vo}).

The Repository will provide the data management functionality required to implement the capabilities described in \Cref{sec:features:drm}.
In particular, this will include support for the concept of ownership and rights to data.
That is, when appropriate, it will be clear what organization produced and is responsible for a piece of data (for example, the \gls{ILT}), and it will be possible to define and enforce policies regarding which users have permission to access it.
These capabilities will be generic: it will be possible to use them to implement the policies of multiple data owners where appropriate.


\subsubsection{Data Processing Service}
\label{sec:product:services:dataprocessing}

The Data Processing Service will provide a generic way of applying defined processing workflows --- or ``pipelines'' --- to input datasets using underlying computing infrastructure.

The Processing Service is intentionally generic with regards to the pipeline being executed: it is expected that some pipelines are defined (\Cref{sec:features:standardpipes}) and operated (\Cref{sec:features:srdp}) by \gls{SDCO} staff, while others are defined and run by end users without direct \gls{SDCO} intervention.
This section describes only the capabilities of the Service itself: it does not discuss the implementation of particular pipelines.
Refer to \cref{sec:product:science:pipelines} for a discussion of the pipelines that will be provided within the \gls{SDC} facility.

It is clear that user-controlled execution of user-provided pipelines presents resource management challenges.
We therefore propose to focus first on developing a robust service which executes standardized pipelines under direct \gls{SDCO} management, with user-control as a future extension.

Initially, we envision pipelines consisting of a series of executable stages, with the relationships between them being described by the \gls{CWL}.
Data is exchanged between pipeline stages using the filesystem.
In future, the Data Processing Service will evolve to remain current with best-practice for defining pipelines developed at both ASTRON and the \gls{SKA} (e.g. potentially supporting smaller units of execution, and in-memory or \gls{RDMA} data transfers).

As discussed in \cref{sec:product:services:repo}, the physical infrastructure may be distributed over multiple physical locations.
In so far as it is practical, this should be abstracted away from the end user.
The Data Processing Service will cooperate with the Data Product Repository to ensure that, where appropriate, computing is scheduled close to the data, to minimize latency and data transport costs.

The underlying infrastructure will likely be heterogeneous: it will provide a mixture of high- and low-memory per core systems as well as accelerators like \glspl{GPU}, with the detailed mix of hardware to be determined by the \gls{SDCO} team based on their assessment of user needs.
Pipeline definitions will describe the sorts of resources which are needed to properly run the processing, and the Service will take this into account when scheduling work.

The Data Processing Service will provide an interface which enables members of the \gls{SDCO} team to schedule pipeline runs automatically based on the requests made in the awarded observing proposals.
Later, it should also provide scheduling of user-defined pipelines, subject to quotas.

\subsubsection{Portal}
\label{sec:product:services:portal}

The Portal is the primary way in which end users will connect to the \gls{SDC}.
It will be accessed through the web, and will provide a number of capabilities, including:

\begin{itemize}
\item user registration and log-in;
\item access to resource calculators and to submit and manage proposals for requesting observing and processing resources at the instruments;
\item facilities for browsing and querying the available datasets (ADEX, the “Data Explorer”);
\item facilities for staging and downloading data\footnote{Multiple alternate download services may be offered to address different data volume and latency requirements.};
\item an interactive analysis environment, which can access data staged from, and write results back to, the Data Product Repository (subject to quota);
\item the ability to schedule jobs to run on the Data Processing Service (subject to quota);
\item the ability to manage and share user-generated datasets;
\item access to user facing documentation of all levels, from introductory tutorials, through reference manuals, to API documentation;
\item access to support.
\end{itemize}

The interactive analysis environment will likely be based on a platform such as JupyterLab\footnote{\url{https://jupyterlab.readthedocs.io/}. Developing a bespoke system of similar capability for the SDC would be implausibly ambitious, but the SDC team will monitor the landscape of available off-the-shelf tooling and may adopt a different solution which provides broadly similar capabilities if that seems appropriate.}, which provides a web-based user interface including interactive notebooks, text editing, interactive terminals, etc.
This will be augmented with a selection of pre-installed tools and libraries drawn from the Application Repository.
It will be possible to initialize the environment with data selected from the Data Explorer, and the environment will provide APIs to enable users to schedule jobs to run on the Data Processing Service.
In this way, it will be possible for the user to make some relatively crude initial data selection using a point-and-click interface, winnow it down interactively in a notebook to select data of real value, and then schedule a pipeline to run on just this valuable data.

Note that providing a useful environment for end users to perform interactive analysis implies that at least some modest CPU and filesystem quota should be made available to them on login.
Likely this filesystem should be persistent.

Ultimately, we have the ambition of facilitating collaboration between users logged in to the portal. In the simplest form, this will entail users being able to collaborate on shared data repositories.
In the longer term, a collaborative development environment\footnote{e.g. \url{https://repl.it}}could be a useful addition.
However --- like the interactive analysis environment --- we suggest that the role of the SDC team here will primarily be in selecting, integrating, and deploying off-the-shelf technologies: a completely bespoke system would be beyond our resources.

Although the SDC will only be directly connected to ASTRON’s data holdings, it will be possible for users to make use of Virtual Observatory tools and standards to access data holdings worldwide, enabling multi-facility and multi-messenger analysis.
Further, by using technologies such as \acrshort{SAMP}~\autocite{2012ivoa.spec.1104T}, it will be possible for users to integrate \gls{VO}-aware analysis tools, such as TOPCAT and Aladin, hosted on their local systems, into their \gls{SDC}-based analysis workflows.

\subsection{Scientific Data Processing}

\subsubsection{Pipelines}
\label{sec:product:science:pipelines}

Key user goals for the \gls{SDC}, as described in \Cref{sec:users}, include enabling the \gls{SDCO} team to generate science-ready data products as part of their regular workflow, and enabling end-users to trigger pipeline processing with customized configuration to address their own particular science needs.

This processing will be carried out by the Data Processing Service, as described in \cref{sec:product:services:dataprocessing}.
This service provides a generic capability for executing processing pipelines, where those pipelines consist of a collection of executable tools --- or stages --- connected together to form a workflow using a system like the \gls{CWL}.

The pipelines which will be executed by the Data Processing Service constitute separate deliverables.
Ultimately, these are expected to come from three sources:

\begin{itemize}
\item The SDC Program will be responsible for developing pipelines which provide the core science-ready data products identified by the SDCO team, based on community input, as being of the widest interest and of having the highest scientific output.
\item Users will be able to reconfigure the core SDC pipelines to address their particular science cases.
      This reconfiguration may consist of simply changing configuration (or individual pipeline stages (e.g. changing imaging/calibration parameters), or perhaps of reordering the stages within an existing pipeline.
\item Users will be able to provide their own pipeline stages (e.g. by uploading executable code) and combine them to generate entirely new pipelines.
\end{itemize}

The \gls{SDC} team will publish documentation and \gls{API} specifications so that user-defined pipelines have access to the same capabilities as the core SDC pipelines, although they may be subject to different compute, storage, or data-transport quotas.

This capability will be rolled out in a staged way, following the ordering given above.
That is, we regard it as of the highest priority to provide a way for the \gls{SDCO} team to generate standardized data products, while enabling user-defined pipelines will follow later in development.

This vision does not specify exactly which pipelines will be developed by the \gls{SDC} team.
Rather, we expect this to evolve based on community inputs and priorities expressed by stakeholders.
Initial work will focus on developing \gls{LOFAR} imaging capabilities.

\subsubsection{Quality Control}

In general, all \gls{SDC} processing will result in the generation of ``metrics'' which can be used in assessing the quality of the results.
For example, metrics might include quality-of-fit parameters, image noise, dynamic range, and so on; the complete list will vary with the type of processing being performed, and will be defined jointly by the pipeline developers and the \gls{SDCO} team.
In some cases, these metrics may be augmented by manual inspection of key data products by the \gls{SDCO} team.
These metrics will be used to add flags to the resulting data products to help users browsing the archive to select data products that are appropriate to their scientific applications.

The \gls{SDC} has particular responsibilities in handling data generated by \gls{LOFAR}, as described in \cref{sec:mission}.
In particular, responsibility for data quality assessment will be shared between \gls{LOFAR} and the \gls{SDC}.
The quality of the raw data and of the products generated on the CEP4 cluster is the responsibility of Telescope Operations, while that of products generated within the \gls{SDC}  will be the responsibility of \gls{SDCO}.
The data quality assessment is based on the intrinsic properties of the data as shown in the validation plots that are automatically generated after the observations and as part of the pipeline processing.
Therefore, the \gls{SDC} data quality assessment will complement that of \gls{LOFAR}.
Based on the quality assessment, observations are classified as successful or failed, following specific policies.
In the former case, the data is sent to the \gls{SDC} for further processing.

\subsection{Key Ancillary and Support Services}

\subsubsection{User Management}
\label{sec:product:supporting:user}

As described above, the primary mechanism by which users will access the \gls{SDC} is through the Portal.
The current design does not explicitly allow for alternative access mechanisms, such as \gls{SSH} or \gls{FTP}, but these are not ruled out for the future.
We also anticipate that \gls{SDCO} make enter into special arrangements with partner facilities for bulk data transfers on an as-needed basis.

The \gls{SDC} will be open to users from all around the world.
However, a system of compute, storage, and data transfer quotas will be imposed to ensure that the system can be operated within a defined budget.

Some services --- for example, browsing the available data holdings --- will be made available to users without logging in.

A user management interface will be provided to the SDCO team as part of the SDC system.

\subsubsection{Documentation}

Providing documentation appropriate to all levels of users is a core goal of the \gls{SDC} effort (\Cref{sec:features:docs}).
Documentation will be tightly integrated with and published through the Portal.

Responsibility for generating documentation is shared by all aspects of the \gls{SDC}.
In particular, developers are responsible for providing \gls{API} and code documentation to accompany their software, while the operations team should provide higher-level guides and descriptions of the instruments, available datasets, processing pipelines, and analysis techniques.

\subsubsection{Communications Channels}

The SDCO team has a number of channels for communicating with the user base, as described in \Cref{sec:features:docs}.
These include, for example, mailing lists, a help desk, and user forums; more functionality along similar lines may be conceived in the future.
While communications of this type form an important part of the \gls{SDC}, we expect them fundamentally to involve deploying and curating off-the-shelf solutions rather than bespoke, \gls{SDC}-specific development.

\subsubsection{Software Distribution}

Although the primary goal of the \gls{SDC} is to provide an operational facility, we regard the software products that the team will write are intrinsically important and worthy of independent distribution (\Cref{sec:features:software}).
All software developed by the ASTRON team for the \gls{SDC} will be released under the terms of the ASTRON Open Source Policy, and, where appropriate, we will aim to foster its use within the wider community.
