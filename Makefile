DOCNAME=SDC-006
export TEXMFHOME ?= astron-texmf/texmf

$(DOCNAME).pdf: $(DOCNAME).tex meta.tex changes.tex
	xelatex $(DOCNAME)
	makeglossaries $(DOCNAME)
	biber $(DOCNAME)
	xelatex $(DOCNAME)
	xelatex $(DOCNAME)

include astron-texmf/make/vcs-meta.make
include astron-texmf/make/changes.make
