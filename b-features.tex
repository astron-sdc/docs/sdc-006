% Frequently used throughout this section
\newcommand{\dps}{Data Processing Service (\cref{sec:product:services:dataprocessing})}
\newcommand{\repo}{Data Product Repository (\cref{sec:product:services:repo})}
\newcommand{\portal}{Portal (\cref{sec:product:services:portal})}

\section{Capabilities}
\label{sec:features}

A key part of the analysis described in \cref{sec:analysis} is to map user needs, as described in \cref{sec:users}, to specific \gls{SDC} capabilities.
That mapping is shown in \cref{tab:userfnmap} (\cpageref{tab:userfnmap}), while this section provides details of each capability.

Given resource constraints, it is clearly not possible to develop every possible capability simultaneously.
We therefore provide a brief discussion of the relative priority of each feature.
This is, in part, derived from the user priorities described in \cref{sec:users}, but also incorporates other information such as relationships between capabilities and the needs of the development team.
Note that describing a feature as low priority is not intended to indicate that it is regarded as unimportant; rather, that it will not be addressed in the first tranche of \gls{SDC} development.

This section also identifies which of the various services mentioned in the \gls{SDC} system design (\cref{sec:product}) are necessary to provide the capabilities being described.

\subsection{Discovery and Access}
\label{sec:features:data}

\subsubsection{Data Discovery}
\label{sec:features:discovery}

\paragraph{Overview}

Data discovery provides users with a fast, convenient, user-friendly interface to help them locate the data products they need.
Data products will be searchable by a variety of metadata and provenance information.
A single interface will be provided to search all data collections available in the \gls{SDC}, with specializations provided for particular instrumentation or types of data product where applicable.
When discovered, data will be made available for download (\cref{sec:features:srdp,sec:features:raw,sec:features:simple}) or made available for on-line interactive analysis (\cref{sec:features:interactive}).

\paragraph{Prioritization}

This functionality is essential to successful \gls{SDC} operations, and must be included in early development plans.

\paragraph{System Design}

The \portal{}  will provide a flexible user interface for querying data collection.
The \repo{}, and, in particular, the supporting Science Data Repository service will provide metadata and indexing capabilities in support of this interface.

\subsubsection{Access to Instrumental Data}
\label{sec:features:raw}

\paragraph{Overview}

Users should be able to access raw (L0\footnote{We classify data products according to their ``calibration level'', following the \gls{IVOA} ObsCore standard \autocite{2017ivoa.spec.0509L}.}) and instrumental (L1) data for download or on-line interactive analysis.
These data products should be accompanied by metadata that describe how they were produced (i.e., ``provenance''), and an assessment of the data quality.
Downloads may be conducted on a per-file basis (e.g. to support a single user who needs access to the data on their local system) or in bulk (e.g. to transfer to another data centre or an external group like an \Acrlong{KSP}).

\paragraph{Prioritization}

This functionality is essential to successful \gls{SDC} operations, and must be included in early development plans.

\paragraph{System Design}

Instrumental data is stored in the \repo{}.
The Staging and Data Transfer services are used to make it available to users on demand.
User requests are submitted, and data is provided for download through, the \portal{}.

\subsubsection{Access to \Acrlongpl{SRDP}}
\label{sec:features:srdp}

\paragraph{Overview}

\glspl{SRDP} data products that the end user can immediately apply to address particular science questions without the need for further calibration or other processing.
In the context of the \gls{SDC}, they are the results of processing instrumental data (\cref{sec:features:raw}) through standardized data reduction pipelines produced and operated by ASTRON and its collaborators.
For example, these might include the results of processing LOFAR data through a standard imaging pipeline.
\glspl{SRDP} must be accompanied by metadata that describe how they were produced (i.e., ``provenance''), and an assessment of the data quality.
They should be made available for download or on-line interactive analysis.

\glspl{SRDP} exist at level 2 or higher on the \gls{IVOA} ObsCore classification scale \autocite{2017ivoa.spec.0509L}.

\paragraph{Prioritization}

This functionality is essential to successful \gls{SDC} operations, and must be included in early development plans.

\paragraph{System Design}

Instrumental data is stored as per \cref{sec:features:raw}.
\gls{SDC} staff are responsible for using the \dps{} to process this data through a range of pre-defined pipelines (\cref{sec:product:science:pipelines}; the level of automation of this process is to be determined).
Data products generated as a result of custom processing by advanced or expert users may optionally be shared and used as \glspl{SRDP} by others; potentially, \gls{SDC} staff could work with experts to identify useful products and flag them to other users.
Processed data products are archived in the \repo{} and made available in the same way as instrumental data (\cref{sec:features:raw}).

\subsubsection{Access to Simplified Data Products}
\label{sec:features:simple}

\paragraph{Overview}

Simplified data products are designed for consumption by the general public.
Like \glspl{SRDP}, they are produced by standardized pipelines, but these pipelines are designed to maximise the appeal of the results to non-specialists.
For example, this could include choosing aesthetically appealing colour maps, omitting complex features, or adding additional labelling.
They should be made available for download or on-line interactive analysis.

\paragraph{Prioritization}

This functionality is necessary to support public outreach efforts, which are not a core \gls{SDC} goal (\cref{sec:goals:users:education,sec:goals:users:public}).
It will be deferred for later in \gls{SDC} development.

\paragraph{System Design}

As for \glspl{SRDP} (\cref{sec:features:srdp}).

\subsubsection{Sharing}
\label{sec:features:sharing}

\paragraph{Overview}

The results of both inactive analysis (\cref{sec:features:interactive}) and pipeline execution (\cref{sec:features:standardpipes,sec:features:custompipes}) may be stored for future use.
Fully exploiting the data, though, will mean that scientists need to share those results with colleagues.
This may take place at the small scale --- individual users inviting another to collaborate on a particular dataset --- or in the large -- a user-generated dataset published to the world, with an associated \gls{PID} and long-term curation.

\paragraph{Prioritization}

This work is not essential to early \gls{SDC} operations, but will form a major part of future development.

\paragraph{System Design}

Sharing data has implications for multiple \gls{SDC} services.
In particular, it must be possible to track permissions to data both while it is stored in the \repo{} and when it has been staged and made available for interactive analysis through the \portal{}.
Background services will provide \gls{PID}-minting and other capabilities.

\subsubsection{\Acrlong{VO} Interfaces}
\label{sec:features:vo}

\paragraph{Overview}

It is possible to use \gls{VO} interfaces both to access data stored within the \gls{SDC}, and --- when using the interactive analysis facilities described in \cref{sec:features:interactive} --- to fetch data from remote sites to use in processing.

\paragraph{Prioritization}

Adopting \gls{VO} interfaces early in \gls{SDC} development will capitalize on existing work in this area already undertaken at ASTRON, and provide a solid technological basis upon which to build other \gls{SDC} functionality.

\paragraph{System Design}

\gls{VO} interfaces will be provided by the \repo{} and the supporting Virtual Observatory service.

\subsubsection{Data Rights Management}
\label{sec:features:drm}

\paragraph{Overview}

\gls{SDC} systems should track the ownership of data products.
For example, facilities may claim ownership of their instrumental and science-ready data products (\cref{sec:features:raw,sec:features:srdp}), while scientists or other end users might own data products produced by custom pipelines (\cref{sec:features:custompipes}) or their own interactive analysis (\cref{sec:features:interactive}).
It is understood that policies around ownership may vary from facility to facility (for example, the \Acrshort{ILT} Board may take a different attitude to the \gls{SKAO}); \gls{SDC} systems must be flexible enough to accommodate all reasonable policies.

The owners of data products must be able to define access rights to the data.
This would include, for example, limiting access to certain individuals during a proprietary period.

\paragraph{Prioritization}

Basic data rights management is essential even in early \gls{SDC} operations: nobody should be able to access data that they don't have permission to see.
The system will grow in complexity and flexibility with time, e.g. to accommodate the results of user-generated processing.

\paragraph{System Design}

As for \cref{sec:features:sharing}, all data access services --- including both the \repo{} and the \portal{} --- must be capable of tracking data rights and mediating access based on user permissions.

\subsection{Processing and Analysis}

\subsubsection{Access to Standard Data Analysis Tooling}
\label{sec:features:standardtooling}

\paragraph{Overview}

Standard data analysis tooling are packages widely used in the wider (radio) astronomy community for data analysis.
This might include, for example, CASA\footnote{\url{https://casa.nrao.edu}}, TOPCAT\footnote{\url{http://www.star.bris.ac.uk/~mbt/topcat/}}, Aladin\footnote{\url{https://aladin.u-strasbg.fr}}, among a wide range of other packages.
Users are familiar with these packages and know how to use them to quickly obtain the results they need.
These tools might be access directly within the \gls{SDC} (e.g. in a web-based environment, or running on a \gls{VM}), or may be packaged for convenient download and offline use.
The tooling should interoperate seamlessly with the data products provided by the \gls{SDC} (\cref{sec:features:srdp,sec:features:raw,sec:features:simple}): having obtained the software and the data through \gls{SDC}-sanctioned channels, they should immediately be able to load and work with the data in their tool of choice without further adaptation.

\paragraph{Prioritization}

This functionality is important to meeting the long-term goals of the \gls{SDC}, but is a second-order functionality which may be deferred until later in development.

\paragraph{System Design}

Direct access to tooling will be provided through the Application Repository service (\cref{sec:product:arch}).
This must be properly integrated with both the \portal{} and the \dps{}, where these tools will be applied.

\subsubsection{Access to Standardized Data Reduction Pipelines}
\label{sec:features:standardpipes}

\paragraph{Overview}

Standardized data reduction pipelines provide algorithms, configuration, and an execution framework which is capable of ingesting instrumental data (\cref{sec:features:raw}) and producing \glspl{SRDP} (\cref{sec:features:srdp}).
Access to these pipelines means that users will be able to trigger reprocessing of instrumental data with a modified configuration to generated data products that are more closely aligned with their scientific needs.
It follows that the products of user-requested pipeline processing may need to be stored for future reference.
Note that pipeline products may consist of binary ``blobs'' (e.g. image files) or tabular data (database entries); it should be possible for users to generate and store both.

\paragraph{Prioritization}

Note that the pipelines themselves must exist early in development, per \cref{sec:features:srdp}.
However, while providing user access to pipeline execution is important to meeting the long-term goals of the \gls{SDC}, it is second-order functionality which may be deferred until later in development.

\paragraph{System Design}

For this purpose, Pipelines are a specific instance of standard tooling; the same considerations as \cref{sec:features:standardtooling} apply.
In addition, this implies that the Portal will provide features to enable users to launch jobs on the \dps{}.

\subsubsection{Define and Run Custom Data Reduction Pipelines}
\label{sec:features:custompipes}

\paragraph{Overview}

Custom data reduction pipelines provide users with the capability to bring their own novel algorithms and execute them within the framework used by the \gls{SDC}, with access to (broadly) the same resources, data sources and sinks, etc, as the standardized pipelines (\cref{sec:features:standardpipes}).
It is likely that users will wish to store the products of custom processing for future reference, publication, etc.
Note that pipeline products may consist of binary ``blobs'' (e.g. image files) or tabular data (database entries); it should be possible for users to generate and store both.

\paragraph{Prioritization}

This is a long-term \gls{SDC} goal which will maximise the scientific output of the facility.
However, it is not necessary to achieve an initial operational capability.

\paragraph{System Design}

All of the same consideration as for standard pipelines apply (\cref{sec:features:standardpipes}).
In addition, this implies that the \dps{} \gls{API} will be documented and advertised to users.

\subsubsection{Interactive Analysis Facilities}
\label{sec:features:interactive}

\paragraph{Overview}

Tools are provided so that the user can perform real-time interactive analysis of data within the \gls{SDC}.
For example, this may include a literate programming (``notebook'') interface, like Jupyter\footnote{\url{https://jupyter.org}}, or a functional command-line environment, to which users can connect, issue commands, and receive results interactively.
The environment should have access to appropriate analysis tooling, and be able to access data held within the \gls{SDC}.
The interactive environment may also be a convenient jumping-off point for standardized or custom pipeline execution (\cref{sec:features:standardpipes,sec:features:custompipes}).
The user will require short-term storage (``scratch space'') in support of ongoing data analysis, and long-term persistent storage for valuable results.

\paragraph{Prioritization}

This work is not essential to early \gls{SDC} operations, but will form a major part of future development.

\paragraph{System Design}

Interactive analysis is a key component of the \portal{}.
Archiving of results will be delegated to the \repo{}.

\subsubsection{Access to Specialist Hardware}
\label{sec:features:hardware}

\paragraph{Overview}

Users may request that custom pipelines be run on non-standard hardware configurations (e.g. \glspl{GPU}, \glspl{FPGA}, etc).

\paragraph{Prioritization}

This work is technically demanding, and is not required for core \gls{SDC} operations.

\paragraph{System Design}

The current \gls{SDC} architecture makes no specific provision for this capability.
Ultimately, it could be supported by the \dps{}.
Care will therefore be taken to ensure this service provides an abstraction layer over the underlying hardware which can easily be extended to cover additional hardware types.

\subsubsection{Real-Time Processing of Streaming Data}
\label{sec:features:realtime}

\paragraph{Overview}

Rather than writing data to persistent storage and then (manually or automatically) scheduling a pipeline to process it, this mode would process data in real-time as it streams to the \gls{SDC}.
For example, a real-time imaging pipeline might enable monitoring of ongoing observations for transients.
Users would be able to  access a stream of images or other data products coming from the telescope at low latency (e.g. seconds).

\paragraph{Prioritization}

In so far as this is required for prompt \gls{QA} by telescope operators some limited functionality may be made available, but scientifically this is not a core goal for early \gls{SDC} development.

\paragraph{System Design}

The current \gls{SDC} architecture makes no specific provision for this capability.
Current \dps{} designs envision it as effectively a batch compute service, with no capability for streaming data processing; likely adding true real-time processing would involve adding a new independent service component.
This may be included in a future \gls{SDC} extension, but not in early development.

\subsection{Documentation and Support}
\label{sec:features:docs}

\subsubsection{Introductory Documentation, Tutorials, and other Resources}
\label{sec:features:basicdocs}

\paragraph{Overview}

To fully engage the community with the \gls{SDC} and ASTRON's data holdings, substantial educational resources must be provided.
These will run the gamut from introductory material on instrumentation and the \gls{SDC} itself, through explanations of the available data and capabilities, to tutorials on data processing.
There will also be support available to users with questions, and a range of in-person and online workshops available to users.
Finally, peer-to-peer support and community development will be facilitated by discussion forums and similar tools.

\paragraph{Prioritization}

This is fundamental to enable the community to engage with the facility, and is therefore of the highest priority.

\paragraph{System Design}

The \gls{SDC} will make extensive use of off-the-shelf tooling for providing these capabilities.
As appropriate, these will be integrated with the \portal{}.

\subsubsection{Technical and \Acrshort{API} Documentation}
\label{sec:features:techdocs}

\paragraph{Overview}

In order to facilitate advanced data processing (\cref{sec:features:custompipes}), technical and \gls{API} documentation will be made available so that users can integrate their own tools and software with \gls{SDC} systems.

\paragraph{Prioritization}

This is a moderately high priority to enable community and inter-facility collaboration.
It is also essential to the \gls{SDC} development workflow.

\paragraph{System Design}

The \gls{SDC} will make extensive use of off-the-shelf tooling for providing these capabilities.
As appropriate, these will be integrated with the \portal{}.

\subsubsection{Access to Source Code and Software}
\label{sec:features:software}

\paragraph{Overview}

The software developed for the SDC will provide an important resource for the community.
This is true across the range of software developed: algorithms and visualization tools can be directly used by scientists, while infrastructure and data management services may be built upon by partner institutions and the \gls{SRC} network.
To make this possible, all relevant source code will be made available in an organized, re-usable way.

\paragraph{Prioritization}

This is a moderately high priority to enable community and inter-facility collaboration.
It is also essential to the \gls{SDC} development workflow.

\paragraph{System Design}

All software developed for the \gls{SDC} will be open source, under the terms of the ASTRON Open Source Policy \autocite{dijkema_tammo_jan_2018_3479829}.
Development will take place on publicly-accessible ``software forge'' sites (e.g. ASTRON's GitLab\footnote{\url{https://git.astron.nl}}).
Software releases will be properly managed, including documentation and release notes, and well advertised to the community\footnote{The \gls{SDC} team expects to collaborate with the Software Delivery Competence Group on this work.}.

\subsection{Interaction with Telescope Operations}

\subsubsection{Receive and Handle Alerts from Other Facilities}
\label{sec:features:alerts}

\paragraph{Overview}

Alerts of astronomical transients (or other events) may be received from other facilities using a standard like VOEvent \autocite{2011ivoa.spec.0711S}.
The system is able to receive these alerts and take action in response (e.g. scheduling follow-up observations with an ASTRON-controlled telescope).

\paragraph{Prioritization}

This is not a core goal for early \gls{SDC} development.
We note that some functionality is available through the LOFAR ``responsive telescope'' system\footnote{\url{https://old.astron.nl/radio-observatory/lofar-system-capabilities/responsive-telescope/responsive-telescope}}; at the present time, no new development is planned.

\paragraph{System Design}

There is currently no provision for this capability within the \gls{SDC}, although the architecture could be extended to encompass it in future.

\subsubsection{Proposal Management}
\label{sec:features:proposal}

\paragraph{Overview}

It is possible to manage the complete proposal lifecycle --- submission, review, scheduling, observing, collecting results --- from an interface (or interfaces) provided by the \gls{SDC}.

\paragraph{Prioritization}

This is core \gls{SDC} functionality.

\paragraph{System Design}

This functionality is expected to be provided by integrating existing tooling (e.g. NorthStar\footnote{\url{https://lofar.astron.nl/proposal}} and ongoing developments such as the \gls{TMSS}\footnote{\url{https://support.astron.nl/confluence/display/TMSS/TMSS+Project+Home}} with the \portal{}.
Detailed design work has not been carried out, pending acceptance and resourcing of the proposed \gls{SDC}--\gls{LOFAR}2.0 integration plan \autocite{2020-Gunst-SDC-L2-Architecture} by the ASTRON \gls{MT}.
