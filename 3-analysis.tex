\section{Use Case Analysis}
\label{sec:analysis}

A three-step process has been undertaken to develop the architecture for the \gls{SDC} facility proposed by this document:

\begin{enumerate}

  \item{We have interpreted the mission statement described in \cref{sec:mission} in terms of the needs of a various different categories of users which might be serviced by the \gls{SDC}}.

  \item{We have identified the capabilities the \gls{SDC} must provide in order to meet those needs.}

  \item{We have produced a sketch of an design for the \gls{SDC} facility which provides a service-based architecture capable of providing those capabilities.}

\end{enumerate}

This flow is illustrated in \cref{fig:sdc-vision-flow}.
The architecture arrived at in this way is described in \cref{sec:product}, while the remainder of this document provides background material describing the structure and sustainability of the \gls{SDC} effort.

\begin{figure}
\begin{center}
\includegraphics[width=0.72\textwidth]{figures/SDC/sdc-vision-flow}
\end{center}
\caption{%
An example of the flow from the \gls{SDC} mission to a variety of user profiles and their associated needs, then to required capabilities and hence to services which will be provided by the \gls{SDC}.
This figure is illustrative of the process adopted in developing this vision; it does not purport to represent all users, capabilities, or services.
}
\label{fig:sdc-vision-flow}
\end{figure}

Brief descriptions of all of the user profiles considered are provided in \Cref{sec:users}, while the associated capabilities are described in \Cref{sec:features}.
We also attach a priority to each profile and to each capability.
These priorities are not intended evaluate the merits of one science case relative to another: that is certainly outside the scope of this document.
Rather, we attempt to take an holistic view of how they relate to the nascent \gls{SDC}.
For users, this means that serving the widest possible community and enabling the greatest range of science cases is an immediate goal, while technically we first focus on capabilities which are fundamental to proper operation of the facility.

The complete mapping between user profiles and the associated capabilities is summarized in \cref{tab:userfnmap}, while
\Cref{sec:features} describes how each of the capabilities will be provided by the \gls{SDC} architecture described in \cref{sec:product}.

% Rotation: \rot[<angle>][<width>]{<stuff>}
\NewDocumentCommand{\rot}{O{60} O{1em} m}{\makebox[#2][l]{\rotatebox{#1}{#3}}}%

\begin{table}
\begin{center}
\begin{tabular}{l|ccccccccccc}
    & \rot{Non-expert radio (\cref{sec:goals:users:nonexpert})}
    & \rot{Expert radio (\cref{sec:goals:users:expert})}
    & \rot{Multi-$\lambda$ and messenger (\cref{sec:goals:users:multilambda})}
    & \rot{Transient (\cref{sec:goals:users:transient})}
    & \rot{Operators (\cref{sec:goals:users:operator})}
    & \rot{Panels (\cref{sec:goals:users:panel})}
    & \rot{SRC network (\cref{sec:goals:users:src})}
    & \rot{Institutional partners (\cref{sec:goals:users:institutions})}
    & \rot{Education (\cref{sec:goals:users:education})}
    & \rot{Public (\cref{sec:goals:users:public})} \\
    \midrule
    Data Discovery (\cref{sec:features:discovery})         & X & X & X &   &   &   &   &   & X & X \\
    Instrument Data (\cref{sec:features:raw})              &   & X &   &   & X &   &   &   &   &   \\
    Science-Ready Data (\cref{sec:features:srdp})          & X &   & X &   &   &   &   &   & X &   \\
    Simple Data (\cref{sec:features:simple})               &   &   &   &   &   &   &   &   & X & X \\
    Sharing (\cref{sec:features:sharing})                  & X & X & X &   &   &   &   &   & X &   \\
    \Acrshort{VO} Interfaces (\cref{sec:features:vo})      & X & X & X & X &   &   &   & X &   &   \\
    Data Rights                                            & X & X & X &   &   & X &   &   &   &   \\
    Standard Tooling (\cref{sec:features:standardtooling}) & X & X & X &   & X &   &   &   &   &   \\
    Standard Pipelines (\cref{sec:features:standardpipes}) & X & X & X &   & X &   &   &   &   &   \\
    Custom Pipelines (\cref{sec:features:custompipes})     &   & X &   & X & X &   &   &   &   &   \\
    Interactive Analysis (\cref{sec:features:interactive}) & X & X & X &   & X &   &   &   & X &   \\
    Spec. Hardware (\cref{sec:features:hardware})          &   & X &   &   &   &   &   &   &   &   \\
    Real-Time Processing (\cref{sec:features:realtime})    &   &   &   & X & X &   &   &   &   &   \\
    Basic Docs (\cref{sec:features:basicdocs})             & X &   & X &   &   & X &   &   & X & X \\
    Tech Docs (\cref{sec:features:techdocs})               & X & X &   &   & X &   & X & X &   &   \\
    Source Code (\cref{sec:features:software})             & X & X & X &   & X &   & X & X &   &   \\
    Handle Alerts (\cref{sec:features:alerts})             &   &   &   & X &   &   &   &   &   &   \\
    Proposal Management (\cref{sec:features:proposal})     &   &   &   &   &   & X &   &   &   &   \\
    \bottomrule
\end{tabular}
\end{center}
\caption{Mapping of user profiles, as described in \Cref{sec:users}, to supporting capabilities, described in \Cref{sec:features}.
An ``X'' in a table cell indicates that we regard the functionality listed in the corresponding row of being important to the user listed in the corresponding column.
This mapping is indicative: it illustrates the range of users being considered and the range of functionality needed to support them; an empty table cell is not intended to indicate that users of this type could never access or benefit from the relevant functionality.}
\label{tab:userfnmap}
\end{table}

The procedure described in this section is indented to bootstrap the development of the \gls{SDC}: we have considered a range of user profiles and the capabilities that they require to get their work done.
Based on this work, it is possible to start the process of designing the \gls{SDC} and planning for its operation.
However, it is not expected that this analysis be exhaustive or immutable.
Rather, the \gls{SDCO} team will develop a detailed set of use cases and operational concepts that enabled detailed design work to be undertaken and, ultimately, the complete operational model for the \gls{SDC} facility to be developed.
Thus, the work presented here will be substantially augmented and expanded upon by future \gls{SDCO} documentation.
