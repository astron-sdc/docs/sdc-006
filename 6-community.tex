\section{Community Interactions}
\label{sec:community}

This section describes the ways in which the \gls{SDC} will interact with its community.
This includes not only policies around access to the facility and the ways in which data rights are enforced, but also the mechanisms by which feedback from the astronomical community and key stakeholders will be addressed.

\subsection{Access Policy and Data Rights}

We envision the SDC as being accessible to all comers, with no restrictions on who is able to sign up for an account and log in to the basic service.
However:

\begin{itemize}

\item Policies will be defined for the use of resources such as storage, computing, and data transfer: simply having the ability to access the \gls{SDC} will not guarantee users with substantial capacity.

\item Users will have to submit proposals for substantial requests or, when requesting observing time, for the generation of advanced data products through the standard ASTRON pipelines.
      If awarded, these requests will be handled by \gls{SDCO}\footnote{Expert users may be asked to participate in this following the current model of ‘user shared-support’ mode.}, who will deliver the final data products to the users.
      For processing requests within a certain quota (to be defined), users will be able to directly use the \gls{SDC} resources and will not need to go through the proposal submission process.

\item The policy detailing the resource allocation will need further definition beyond its current concept state.
      However, we note here simply that \gls{SDC} systems must be able to effectively implement this policy.

\item Data stored within and made available through the \gls{SDC} will be subject to ownership and data rights policies which will vary based on its origin.
      Again, \gls{SDC} systems must be capable of recognizing and enforcing these policies.

\end{itemize}

Given an increasing move towards a model of cloud-based compute and storage, members of the user community may be able to provision their own compute and/or storage capacity on systems co-located with the SDC.
As such, users could directly access SDC resources at their own cost.
We suggest that this might be a worthwhile avenue for future development, but is not a priority for the SDC at the present time.

\subsection{Community Engagement}
\label{sec:community:engagement}

We regard it as vital that the SDC be responsive to the current and emergent needs of the scientific community and other facilities.
By engaging with them directly, we can best ensure that the SDC is achieving its aims, and plan for and prioritise future work.
We therefore expect the SDC team to maintain regular contact with:

\begin{itemize}
\item the Board of the \gls{ILT};
\item the \gls{LUC};
\item the various \gls{LOFAR} \glspl{KSP};
\item other relevant organizations representing sections of the scientific community;
\item the \gls{SRC} network.
\end{itemize}

Engagement with \gls{ILT} Board is realised through a regular dialogue every trimester.
The \gls{SDCO} team provides updates about the activities relevant to \gls{LOFAR} and receives feedback from the Board concerning the areas that should receive focus in the future.
The \gls{SDCO} also has a regular dialogue with the \gls{LUC} every quarter, during which they offer a comprehensive overview of the achieved results, active development areas, and planning.
Following this, the \gls{LUC} will summarize the feedback from its members, as well as that the \glspl{KSP} it represents, and share it with \gls{SDCO}.
Dialogue with the \gls{SRC} network is realised through the engagement and contribution of various \gls{SDC} representatives to the \gls{SRCSC} working groups.

In addition, we expect to hold a programme of \gls{SDC} schools, traineeships, workshops, and other educational events which will serve not only to inform the community about the capabilities available through the \gls{SDC} but also to provide fora through which the \gls{SDC} team can solicit feedback directly from community members.
