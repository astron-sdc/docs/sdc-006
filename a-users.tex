\section{User Profiles}
\label{sec:users}

Achieving the mission stated in \cref{sec:mission} requires addressing the needs of a wide range of users.
In support of that goal, this section considers a variety of user profiles, providing a brief description of how we expect them to benefit from the \gls{SDC}.
This consideration is necessarily incomplete, given the scope and nature of this document: the aim is to establish the initial parameters for the \gls{SDC} effort, rather than attempt a comprehensive survey of the (potential) user community.
The \gls{SDCO} team will publish detailed user studies over the course of \gls{SDC} development.

A discussion of relative priority is attached to each user profile.
The intention here is not to evaluate the merits of some science cases relative to others, but rather to provide a guide to where we believe the biggest early impacts of the \gls{SDC} can be achieved.
Ultimately, it is our aim to address all user needs wherever possible.

\subsection{Science Users}

\subsubsection{The Non-Expert Radio Astronomer}
\label{sec:goals:users:nonexpert}

\paragraph{Profile}

The non-expert radio astronomer is a user who has a basic understanding of the principles of radio astronomy and who is active in academic astronomy research using radio data, but who is not a specialist in the types of data or instrumentation provided by ASTRON.
They want to easily find and quickly access data products which they can use to get to scientific results with the minimum of overhead.
In general, they are happy with well-accepted ``best practice'' data reduction techniques; they are not interested in developing or applying exotic calibration or imaging techniques.
They might need help in understanding the details of how a particular instrument works, or the subtleties of some data products.
They will be familiar with a range of standard astronomy analysis tooling and want to use it in the course of their research.
They are generally working on their personal laptop, which doesn't have \gls{HPC} or bulk data storage capabilities; they might have access to those facilities through their institution, but they often work while on the move, and dealing with restrictive institutional security policies and complex cluster configurations gets in the way of their science.
Often, they are working as part of a team, and will want facilities to help them collaborate and share results with their colleagues.

\paragraph{Prioritization}

This class of user makes up a large part of the potential \gls{SDC} community, and are known to struggle with the complexities of data products.
Supporting them is regarded as high priority.

\subsubsection{The Expert Radio Astronomer}
\label{sec:goals:users:expert}

\paragraph{Profile}

The expert radio astronomer combines the scientific interests of their peers (\cref{sec:goals:users:expert}) with a deeper understanding of, and interest in, the details of the instrumentation operated by ASTRON.
They often build their own tools and novel algorithms to get the best of the data; in doing so, they can address science cases which are inaccessible using only standardized, lowest-common-denominator data products.
They want to be able to examine instrumental data, and may want to process it in bulk using their own particular recipes; perhaps those recipes are modified or adapted from standard pipelines.
In some cases, they may even need access to specialist hardware like \glspl{GPU} or \glspl{FPGA}.
Experts will often produce novel data products which it may be valuable to share with the wider community.

\paragraph{Prioritization}

This class of user is influential within the community, and may produce results and developing processing techniques that are both exceptionally scientifically interesting and of wide applicability.
Supporting them is regarded as high priority.

\subsubsection{The Multi-Wavelength or Multi-Messenger Astronomer}
\label{sec:goals:users:multilambda}

\paragraph{Profile}

For some science cases, the best results will be achieved by combining measurement made across a variety of different wavelength regimes.
These astronomers are primarily interested in accessing standardized, science-ready data products from ASTRON's instrumentation, and may need support in working with them: the terminology, the form of the data products, the tools used to work with them, and so on may be unfamiliar.
In addition, they will often seek to combine radio data with information from other wavelengths or other messengers.
This combination may be performed either in catalogue-space (by directly comparing or correlating source lists), or by processing and visualizing images from a variety of different instrumentation.

\paragraph{Prioritization}

We regard it as important to provide first-class support to as wide as possible a community of practising astronomers.
In addition, we believe that multi-wavelength and -messenger support has the potential to generate some of the biggest science impacts from the \gls{SDC}.

\subsubsection{The Transient \& Time-Sensitive Astronomer}
\label{sec:goals:users:transient}

\paragraph{Profile}

These astronomers are interested in detecting and responding to astronomical transients with some constrained latency\footnote{This may be at the level of seconds, or perhaps substantially longer --- days or weeks --- but in a regime where simply arranging for follow-up observations through the regular proposal process is not appropriate.}.
These astronomers therefore have broadly two aims:
\begin{itemize}

  \item{To scan raw data being received in near-real-time, identifying and alerting on any transient signals observed;}

  \item{To receive alerts of transients observed by other (non-ASTRON) facilities, and arrange for follow-up observations by ASTRON instrumentation.}

\end{itemize}

\paragraph{Prioritization}

Support for these use cases would involve substantial work on core infrastructure; while we do not question the scientific results possible, the amount of effort involved makes this work hard to prioritize.
We hope to extend \gls{SDC} functionality to better serve this class of user in the future.

\subsection{Administrative and Technical Users}

\subsubsection{Operators of ASTRON's Instrumentation}
\label{sec:goals:users:operator}

\paragraph{Profile}

As discussed in \cref{sec:mission}, the \gls{SDC} will have a uniquely close relationship with LOFAR: while the \gls{SDC} will process and serve data from a range of instrumentation, it will serve as the primary data delivery mechanism for LOFAR, with responsibility for generating advanced data products --- defined as L2+ using the terminology defined by the \gls{IVOA} ObsCore standard \autocite{2017ivoa.spec.0509L} --- lying solely within the \gls{SDC}.
It follows that telescope operators will rely on the \gls{SDC} to provide prompt feedback on the quality of the data being collected and the results of pipeline processing to plan necessary instrument maintenance and other operational tasks.

\paragraph{Prioritization}

It is fundamental to ASTRON's plans that its operators be able to make use of \gls{SDC} capabilities.

\subsubsection{Proposal Review Panels}
\label{sec:goals:users:panel}

\paragraph{Profile}

Again following \cref{sec:mission}, the \gls{SDC} will coordinate the submission, review, selection, and scheduling of observing proposals for LOFAR.
The panels and other bodies involved in this process will need appropriate tooling to manage this process and support their deliberations, as well as status reports and documentation on the status and capabilities of the telescope.

\paragraph{Prioritization}

It is fundamental to ASTRON's plans that the \gls{SDC} be capable of supporting the full proposal lifecycle.

\subsubsection{The \acrlong{SRC} Network}
\label{sec:goals:users:src}

\paragraph{Profile}

The long-term goals of the \gls{SRC} network as it relates to the \gls{SKA} are broadly aligned with the shorter-terms goals of the \gls{SDC} as it relates to ASTRON's current instrumentation.
As such, members of the \gls{SRC} network may be interested in learning from the progress made in the \gls{SDC}, and perhaps in re-using, extending, or adapting some of the \gls{SDC}'s technology stack.
They will therefore be consumers of documentation and software.

\paragraph{Prioritization}

Deep integration with the \gls{SRC} network is important to ASTRON's ambition of directly operating a Regional Centre.

\subsubsection{Institutional Partners}
\label{sec:goals:users:institutions}

\paragraph{Profile}

As with the \gls{SRC} network members (\cref{sec:goals:users:src}), other institutions are working on projects which are aligned with or provide overlapping functionality to the \gls{SDC}.
It is in their interests --- and ours! --- to facilitate easy interoperability between these facilities, to learn from each other, and to share software and other technologies wherever possible.

\paragraph{Prioritization}

It is important both to the \gls{SRC} itself and to our partners to interoperate effectively, but work here must be justified on the basis of scientific return.

\subsection{Education and Outreach}

\subsubsection{Educators, Outreach Specialists, and Teachers}
\label{sec:goals:users:education}

\paragraph{Profile}

Members of the educational community --- broadly defined, but excluding professional astronomy students who likely fall into the category described in \cref{sec:goals:users:nonexpert} above --- may wish to use documentation and services available from the \gls{SDC} in their work.
One could also imagine a professional \gls{SDC} \gls{EPO} programme.

\paragraph{Prioritization}

We will support these users if possible, but do not regard doing so as part of the core mission of the \gls{SDC}.
At this time, we are not resourced or expecting to support an \gls{SDC} \gls{EPO} programme.

\subsubsection{The General Public}
\label{sec:goals:users:public}

\paragraph{Profile}

Members of the general public may wish to learn about ongoing astronomical research for their own education and enjoyment.
They will be interested in simplified data products --- likely primarily images designed to be relatively easily understandable --- rather even than the science-ready data products described above.
They may be interested and eager to participate in citizen science projects, which could directly contribute to professional research efforts.
They will need plenty of introductory information: explanatory material about the underlying astronomy, about how the processing works, and about how to use tools that are familiar to professional astronomers.

\paragraph{Prioritization}

We welcome public engagement with the \gls{SDC}, but do not regard this use case as high priority for the foreseeable future.
